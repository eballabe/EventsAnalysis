## Events Analysis 

 This macro should be used to analyze ATLAS official production of EVEN_NT files <br />
 Pixel Offline Software <br />

  ERIC BALLABENE, eric.ballabene@cern.ch <br />

  DATA AND MC available here: eos/atlas/atlascerngroupdisk/det-ibl/data/PaperRun2/official/EVENT_NT <br />

## To run the program, do in a ROOT session: ##

 .L trktree.C <br />
 .L run.C <br />
 main() <br />
 <br />
 This will produce histograms which are stored in the histos folder. <br />
 
## Choose the events and the samples to run

  e.g. use ana.Loop(fChain,50); for 50 events <br />

## Plotter
   You can plot using:<br />
   plot.C <br />
   and choose which specific histogram you want to see, amongst the following:<br />
   plot_Charge() <br />
   plot_clustersizeX <br />
   plot_clustersizeY <br />
   plot_Residuals <br />
   plot_IncidenceAngle() <br />
   plot_ToT() <br />
   plot_efficiency() <br />
   <br />
   This will take input histograms from the histos folder and display them.